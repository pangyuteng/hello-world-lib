#!/bin/bash

DOCKER_BUILDKIT=1 docker build -t hello-world-lib:latest .
docker tag hello-world-lib:latest pangyuteng/hello-world-lib:latest
echo ${DOCKERHUB_ACCESS_TOKEN} | docker login -u pangyuteng --password-stdin
docker push pangyuteng/hello-world-lib:latest