

```
+ start runner 
https://docs.gitlab.com/runner/install/docker.html#option-1-use-local-system-volume-mounts-to-start-the-runner-container


sudo useradd gitlab-runner
echo $(id gitlab-runner -u):$(id gitlab-runner -g)
sudo usermod -aG docker gitlab-runner

# ??? chmod 666 /var/run/docker.sock

# --env-file .env 
# moved DOCKERHUB_ACCESS_TOKEN to https://gitlab.com/pangyuteng/hello-world-lib/-/settings/ci_cd

docker build -f Dockerfile.gitlab-runner -t my-gitlab-runner .
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    my-gitlab-runner


temporary docker access fix: `sudo chmod 666 /var/run/docker.sock`

alternate fix: run docker executer as user root 
or add user  $(id gitlab-runner -u):$(id gitlab-runner -g) to host and add to docker group


+register runner
https://docs.gitlab.com/runner/register/index.html#docker

docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register

url * token: get it from https://gitlab.com/pangyuteng/hello-world-lib/-/settings/ci_cd
tag: hello-world
executor: shell

# alternatively there is `register --non-interactive`

+ verify and restart runners
docker exec gitlab-runner gitlab-runner verify
docker exec gitlab-runner gitlab-runner restart



```
